// Author: James Gillman
// Title: Crazy Eights Part 5
// Due Date: ?
// Description: Plays a simpler game of Crazy 8s,

#define _CRT_SECURE_NO_WARNINGS  // lets us use depricated code only for Visual Studio

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

//////////////////////
// TOO LAZY DEFINES //
//////////////////////
#define SPADES 128
#define HEARTS 64
#define DIAMONDS 32
#define CLUBS 16
#define DECK_SIZE 52 //Size of standard deck not including Joker.
#define HIGHERBITS 240
#define LOWBITS 15
int deckCards = 52;

//////////////
// TYPEDEFS //
//////////////
typedef unsigned char cardChar;
typedef struct nodeS node; //May not use this, only if I'm lazy, not good for readabilty.


/////////////
// Structs //
/////////////
struct nodeS
{
    unsigned char oneCard;
    struct nodeS *next;
};

struct playerS
{
    int playerID;
    struct nodeS *hand;
};


//////////////
//PROTOTYPES//
//////////////
void instructionals(); //Reads in cards and outputs instructions.
void createDeck(cardChar boardArray[]); //Create a deck and output the value of each spot in the array to prove it worked.
void getCardInfo(cardChar aSingularCard,int infoType); //Prints out card info from a selected card.
void shuffleThoseCards(cardChar boardArray[]); //Does a quick shuffle of boardArray.
cardChar giveMeACardDealer(cardChar deckArray[]); //Grabs a card for player from the top of the card deck.
void createHandList(struct playerS player,cardChar deckArray[]);
void addToHand(struct playerS playerArray[],int playerNum,cardChar deckArray[]);
void playGame(struct playerS player[],cardChar deckArray[]);
cardChar chooseCard(node *head, int cardNum);
void endGame(struct playerS playerArray[]);
cardChar changeSuit(unsigned char playerCard);

/////////////////////
//Helper Prototypes//
/////////////////////
int fclose(FILE *fin); //Closes file.
void printDeck(cardChar boardArray[]); //Prints out the deck, helpful for checking if everything was set up correctly.
int getDeckLocation(); //Gets location of array, kept as a static variable.
void printHand(node *head, int infoType);
int getCardCount(node *head);
void removeCard(node *head, int cardNum);
int doesItMatch(cardChar playerCard, cardChar drawnCard);
int sumPoints(node *head);



//////////
// MAIN //
//////////
int main()
{
    cardChar deckArray[52]; //A standard deck of cards, not including Joker Card.
    struct playerS playerArray[3];

    instructionals();
    createDeck(deckArray);
    shuffleThoseCards(deckArray);

    playerArray[0].hand = NULL;
    //playerArray[1].hand = NULL;
   for (int player = 0; player < 1; player++)
   {
       for(int i = 0; i < 10; i++)
        {
            addToHand(playerArray, player, deckArray);
        }
    }
    playGame(playerArray, deckArray);

    free(playerArray[0].hand);
    free(playerArray[1].hand);

    system("pause"); //FOR VISUAL STUDIO USERS.
    return 0;
}

////////////////////
// MAIN FUNCTIONS //
////////////////////

// Author: James (Ron) Gillman
// Function Name: playGame+
// Parameters: N/A
// Return: N/A
// Description: Plays a game of Crazy 8s
void playGame(struct playerS playerArray[], cardChar deckArray[])
{
    cardChar cardDrawn;
    cardChar playerCard;
    int answer;
    int player = 1;
    int cardNum;
    cardDrawn = giveMeACardDealer(deckArray);

    while(1)
    {
        player = 0;

        printf("Card In Play: ");
        getCardInfo(cardDrawn,0);
        printf("\n");
        printf("Select a card! \n ");

        printHand(playerArray[player].hand,1);
        scanf("%d",&cardNum);
        playerCard = chooseCard(playerArray[player].hand, cardNum);

        if (cardNum == -1)
        {
          printf("DRAWING ANOTHER CARD!\n");
          addToHand(playerArray[player].hand, player, deckArray);
        }
        else if(doesItMatch(playerCard, cardDrawn))
        {
            printf("CARD MATCHES! MAKING IT NEW CARD TO MATCH!\n");

            if((playerCard & LOWBITS) == 8)
            {
                printf("CRAAAAAZY 8s!!! CHANGE SUIT? (YES 1, NO 0)\n");
                scanf("%d", &answer);
                if (answer == 1)
                {
                    playerCard = changeSuit(playerCard);
                }

            }
            cardDrawn = playerCard;
            removeCard(playerArray[player].hand, cardNum);
        }
        else
        {
            printf("DOES NOT MATCH!\n");
            printf("DRAW ANOTHER CARD? (YES 1, NO 0)\n");
            scanf("%d",&answer);
            if(answer == 1)
            {
                addToHand(playerArray, player, deckArray);
            }
        }
        if ( (getCardCount(playerArray[player].hand) == 1) ||  deckCards == 0)
        {
            break;
        }
    }
    endGame(playerArray);
}

// Author: James (Ron) Gillman
// Function Name: endGame
// Parameters: playerArray: array of players
// Return: N/A
// Description: Tallies up points and Declares a winner.
void endGame(struct playerS playerArray[])
{
    int player1Score = sumPoints(playerArray[0].hand);
    int player2Score = sumPoints(playerArray[1].hand);

    if(player1Score > player2Score)
    {
        printf("PLAYER 1 WINS!");
    }
    else
    {
        printf("PLAYER 2 WINS!");
    }
}

// Author: James (Ron) Gillman
// Function Name: playGame
// Parameters: Head: Head of the list of cards, cardNum: selected Card
// Return: CardChar
// Description: Retirieves selected card info;
cardChar chooseCard(node *head, int cardNum)
{
    int ptrNum = 0;
    cardChar card = 0;
    node *cur;
    cur = head;
    while(cur != NULL && ptrNum != cardNum)
    {
        cur=cur->next;
        ptrNum++;
    }

    return cur->oneCard;
}

// Author: James (Ron) Gillman
// Function Name: doesItMatch
// Parameters: PlayerCard: players chosen card, drawnCard: card in play
// Return: int
// Description: Checks to see if card is same as drawnCard, either through type or value;
int doesItMatch(cardChar playerCard, cardChar drawnCard)
{

    cardChar playerAmount = playerCard & LOWBITS;
    cardChar drawnCardAmount = drawnCard & LOWBITS;

    printf("PlayerAmount: %d\n", playerAmount);
    printf("drawnCardAmount: %d\n", drawnCardAmount);

    int playerCardType = playerCard & HIGHERBITS;
    int drawnCardType = drawnCard & HIGHERBITS;

    printf("PlayerCardType: %d\n", playerCardType);
    printf("PlayerCardType: %d\n", drawnCardType);

    if(playerAmount == drawnCardAmount || playerCardType == drawnCardType)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

// Author: James (Ron) Gillman
// Function Name: instructionals
// Parameters: N/A
// Return: N/A
// Description: Uses file input to read instructionals.
void instructionals()
{
    FILE *fin; //pointer for file input.
    char readBuffer[500]; //Array to store line of text from instructions.txt
    readBuffer[499] = '\0';

    //fin = fopen("instructions.txt", "r");
    fin = fopen("/home/safetypanda/Documents/CodingProjects/crazy8s/instructions.txt", "r");
    if (fin == NULL)
    {
        printf("Error, file didn't open\n\nExiting program...\n\n");
        system("pause");
        exit(1);
    }

    do
    {
        fgets(readBuffer, 500, fin);
        printf("%s", readBuffer);
    } while (!feof(fin));

    fclose(fin);
}

// Author: James (Ron) Gillman
// MethodName: getCardInfo
// Parameters: int singularCard: Integer data value of a card.
// Return: NONE
// Description: Reads an integer representation of the card, decodes it and prints out info about the card.
void getCardInfo(cardChar singularCard, int infoType)
{

    ///////////////
    // CARD REFR //
    // 0000 0000 //
    // SHDC 1-13 //
    ///////////////
    if (infoType == 1)
    {
        printf("You have a %d ", (singularCard & LOWBITS));
    }
    else
    {
        printf("%d ", (singularCard & LOWBITS));
    }

    if (singularCard & SPADES)
    {
        printf("Of Spades!\n");
    } else if (singularCard & HEARTS)
    {
        printf("Of Hearts!\n");
    } else if (singularCard & DIAMONDS)
    {
        printf("Of Diamonds!\n");
    } else if (singularCard & CLUBS)
    {
        printf("Of Clubs!\n");
    } else
    {
        printf("You seem lost, you shouldn't be here...\n");
    }
}

// Author: James (Ron) Gillman
// Function Name: createDeck
// Parameters deckArray: array of all cards in game
// Return: NONE
// Description: Recieves an Array of Int and create the deck of cards.
void createDeck(cardChar deckArray[])
{
    for (int i = 0; i < DECK_SIZE; i++)
    {
        for (int j = CLUBS + 1; j < CLUBS + 14; j++)
        {
            deckArray[i] = j;
            i++;
        }

        for (int k = DIAMONDS + 1; k < DIAMONDS + 14; k++)
        {
            deckArray[i] = k;
            i++;
        }

        for (int l = HEARTS + 1; l < HEARTS + 14; l++)
        {
            deckArray[i] = l;
            i++;
        }

        for (int m = SPADES + 1; m < SPADES + 14; m++)
        {
            deckArray[i] = m;
            i++;
        }
    }
}

// Author: James (Ron) Gillman
// Function Name: shuffleThoseCards
// Parameters: int deckArray[]: Standard Deck of 52 cards.
// Return: NONE
// Description: Shuffles Cards in a somewhat nice way.
void shuffleThoseCards(cardChar deckArray[])
{
    srand( time(NULL));
    int randomNum = 0;
    int tempValue = 0;

    for (int j=0; j<DECK_SIZE; j++)
    {
        for (int i=0; i<DECK_SIZE; i++)
        {

            randomNum = rand() % (DECK_SIZE + 1 - 0) + 0;

            tempValue = deckArray[i];
            deckArray[i] = deckArray[randomNum];
            deckArray[randomNum] = tempValue;
        }
    }
}

// Author: James (Ron) Gillman
// Function Name: giveMeACardDealer
// Parameters: deckArray: array of all cards in game
// Return: unsigned int: top card from deck
// Description: Gives player a card
cardChar giveMeACardDealer(cardChar deckArray[])
{
    cardChar topCard = 0; //Location of the "top" card of the deck.
    topCard = deckArray[getDeckLocation()];
    return topCard;
}

// Author: James Gillman
// Function Name: changeSuit
// Parameters: playerCard: players current card
// Return: cardChar
// Description: changes card suit based on player choice.
cardChar changeSuit(unsigned char playerCard)
{
    int answer = 0;
    cardChar playerCardType = playerCard & HIGHERBITS;
    playerCard -= playerCardType;

    printf("Change to?\n");
    printf("[1] SPADES\n");
    printf("[2] HEARTS\n");
    printf("[3] DIAMONDS\n");
    printf("[4] CLUBS\n");

    scanf("%d",&answer);

    if (answer == 1)
    {
        playerCard += SPADES;
    }
    else if (answer == 2)
    {
        playerCard += HEARTS;
    }
    else if( answer == 3)
    {
        playerCard += DIAMONDS;
    }
    else
    {
        playerCard += CLUBS;
    }

    return playerCard;

}

//////////////////////
// HELPER FUNCTIONS //
//////////////////////


// Author: James (Ron) Gillman
// Function Name: getDeckLocation
// Parameters: NONE
// Return: int
// Description: Keeps track of deck location
int getDeckLocation()
{
    static int deckLocation = -1;
    deckLocation ++;
    deckCards--;
    return deckLocation;
}

// Author: James (Ron) Gillman
// Function Name: printDeck
// Parameters: cardChar deckArray: Deck of cards
// Return: NONE
// Description: Prints out deck to make sure things are correct.
void printDeck(cardChar deckArray[])
{
    printf("PRINTING DECK\n");
    for (int i = 0; i < DECK_SIZE; i++)
    {
        printf("Value of deckArray[%d]:", i);
        getCardInfo(deckArray[i],0);
        printf("\n");
    }
}

// Author: James (Ron) Gillman
// Function Name: printDeck
// Parameters: head: head of list of cards in hand. infoType: Whether or not they want full info.
// Return: NONE
// Description: Prints out players hand
void printHand(node *head, int infoType)
{
    int count = 0;
    node *current = head;

    while(current != NULL)
    {
        printf("[%d] ", count);
        getCardInfo(current->oneCard, infoType);
        printf("\n");
        current = current->next;
        count++;
    }
}

// Author: James (Ron) Gillman
// Function Name: getCardCount
// Parameters: head: head of list of cards in hand
// Return: int
//Description: Gets amount of cards in hand
int getCardCount(node *head)
{
    node *cur;
    cur = head;
    int cardCount = 0;

    while(cur != NULL)
    {
        cur=cur->next;
        cardCount++;
    }

    if (cardCount > 0)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

// Author: James (Ron) Gillman
// Function Name; addToHand
// Parameters: playerArray: array of players. playernum: which player is playing?, deckArray: array of all cards in "deck"
// Return: N/A
// Description: Adds card to players hand
void addToHand(struct playerS playerArray[],int playerNum,cardChar deckArray[])
{
    node *newNode;
    newNode = malloc(sizeof(node));
    newNode->oneCard = giveMeACardDealer(deckArray);

    newNode->next = playerArray[playerNum].hand;
    playerArray[playerNum].hand = newNode;
}

// Author: James (Ron) Gillman
// Function Name: removeCard
// Parameters: Head: Head of the list of cards, cardNum: selected Card
// Return: N/A
// Description: Removes Selected Card from Hand
void removeCard(node *head, int cardNum)
{
    int ptrNum = 0;
    node *cur, *prev;
    node *temp;
    cur = head;
    prev = head;
    while(cur != NULL && ptrNum != cardNum)
    {
        cur=cur->next;
        ptrNum++;
    }
    while(prev->next != cur)
    {
        prev = prev->next;
    }

    prev->next = cur->next;

    free(cur);
}

//Author: James Gillman
//Function Name: sumPoints
//Parameters: head: head of players hand list
//Return: int, sum of points
//Description: Goes through list and adds up points
int sumPoints(node *head)
{
    node *cur;
    cur = head;
    int cardSum = 0;

    while(cur != NULL)
    {
        cardSum += (cur->oneCard & LOWBITS);
        cur=cur->next;

    }

    return cardSum;
}

//PROBLEMS: LINK LIST STILL GOOFING UP. WORKS FIN ON FIRST, SECOND NO NO. NOT DELETING NODES PROPERLY...

